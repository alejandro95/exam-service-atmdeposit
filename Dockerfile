FROM maven:3.6-jdk-8-alpine AS builder
VOLUME /tmp
ADD ./target/exam-service-atmdeposit-0.0.1-SNAPSHOT.jar service-atmdeposit.jar
ENTRYPOINT ["java","-jar","/service-atmdeposit.jar"]