package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamServiceAtmdepositApplication {

  public static void main(String[] args) {
    SpringApplication.run(ExamServiceAtmdepositApplication.class);
  }

}
