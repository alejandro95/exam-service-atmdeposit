package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.client;

import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response.AccountResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AccountApi {


  @GET("/core/accounts")
  Single<AccountResponse> obtainAccount(@Query(value = "cardNumber") String cardNumber);


}
