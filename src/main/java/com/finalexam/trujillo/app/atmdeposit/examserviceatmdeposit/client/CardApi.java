package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.client;

import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response.CardResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CardApi {


  @GET("/core/cards")
  Single<CardResponse> obtainCard(@Query(value = "documentNumber") String documentNumber);


}
