package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.client;

import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.request.FingerPrintRequest;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response.FingerPrintResponse;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface FingerprintApi {

  @POST("/core/fingerprints/validate")
  Single<FingerPrintResponse> validateFingerprint(@Body FingerPrintRequest fingerPrintRequest);

}
