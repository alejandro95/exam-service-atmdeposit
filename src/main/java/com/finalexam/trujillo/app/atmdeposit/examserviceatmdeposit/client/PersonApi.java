package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.client;

import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response.PersonResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;



public interface PersonApi {

    @GET("/core/persons/{documentNumber}")
    Single<PersonResponse> getPerson(@Path(value = "documentNumber") String documentNumber);

}
