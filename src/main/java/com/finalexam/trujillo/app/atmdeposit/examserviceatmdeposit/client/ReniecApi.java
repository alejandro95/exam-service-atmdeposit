package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.client;

import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.request.ReniecRequest;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response.ReniecResponse;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ReniecApi {


  @POST("/external/reniec/validate")
  Single<ReniecResponse> validateReniec(@Body ReniecRequest reniecRequest);

}
