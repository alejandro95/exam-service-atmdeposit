package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.controller;

import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.request.PersonRequest;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response.AtmResponse;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.service.IAtmService;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/atm")
public class AtmController {

  @Autowired
  private IAtmService tmService;

  /**
   * Controlador para asdasdasdasd
   *
   * @param personRequest {@link DeleteRequest}
   * @return Service Response ({@link AtmResponse})
   */
  @PostMapping("/deposits/")
  @ResponseStatus(HttpStatus.CREATED)
  public Single<AtmResponse> listDeposit(@RequestBody PersonRequest personRequest) {

    return tmService.depositsAtm(personRequest.getDocumentNumber());


  }


}
