package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Builder
public class Card {

  private String cardNumber;
  private Boolean active;

}
