package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SuccessValidation {
  private String entityName;
  private Boolean success;
}
