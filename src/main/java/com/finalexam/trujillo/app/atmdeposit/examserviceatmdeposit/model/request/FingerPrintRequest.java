package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FingerPrintRequest {

    private String document;
}
