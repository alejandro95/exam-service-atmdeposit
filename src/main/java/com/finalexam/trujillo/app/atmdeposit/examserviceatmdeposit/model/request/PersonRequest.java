package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.request;


import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonRequest {


  private String documentNumber;

}
