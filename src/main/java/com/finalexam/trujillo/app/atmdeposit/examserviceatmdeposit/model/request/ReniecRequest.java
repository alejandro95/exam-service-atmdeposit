package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.request;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReniecRequest {

    private String document;
}
