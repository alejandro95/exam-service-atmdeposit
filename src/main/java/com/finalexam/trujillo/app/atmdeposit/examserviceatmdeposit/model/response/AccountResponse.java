package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Builder
public class AccountResponse {


  private String accountNumber;
  private Double amount;
}
