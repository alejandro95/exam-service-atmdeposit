package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AtmResponse {

  private String fingerprintEntityName;
  private List<ValidAccountResponse> validAccounts;
  private Double customerAmount;
}
