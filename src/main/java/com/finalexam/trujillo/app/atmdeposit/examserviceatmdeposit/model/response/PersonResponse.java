package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Builder
public class PersonResponse {


  private String id;
  private String document;
  private Boolean fingerprint;
  private Boolean blacklist;

}
