package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class ReniecResponse {

  private String entityName;
  private Boolean success;

}
