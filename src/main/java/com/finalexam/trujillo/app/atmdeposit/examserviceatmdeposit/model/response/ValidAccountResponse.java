package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ValidAccountResponse {
  private String accountNumber;
}
