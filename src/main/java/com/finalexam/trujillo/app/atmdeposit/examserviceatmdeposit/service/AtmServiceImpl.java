package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.client.*;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.entity.Card;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.entity.SuccessValidation;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.request.FingerPrintRequest;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.request.ReniecRequest;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response.*;
import io.reactivex.Flowable;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class AtmServiceImpl implements IAtmService {

  @Autowired
  private PersonApi personApi;

  @Autowired
  private FingerprintApi fingerprintApi;

  @Autowired
  private ReniecApi reniecApi;

  @Autowired
  private CardApi cardApi;

  @Autowired
  private AccountApi accountApi;

  private Single<PersonResponse> validatePerson(String documentNumber) {

    return personApi.getPerson(documentNumber);
  }

  private Single<FingerPrintResponse> validateFinger(String documentNumber) {

    return fingerprintApi.validateFingerprint(
        FingerPrintRequest.builder()
            .document(documentNumber)
            .build());

  }

  private Single<ReniecResponse> validateReniec(String documentNumber) {
    return reniecApi.validateReniec(ReniecRequest.builder().document(documentNumber).build());
  }

  private Single<CardResponse> obtainCard(String documentNumber) {
    return cardApi.obtainCard(documentNumber);
  }

  private Single<AccountResponse> obtainAccount(String cardNumber) {
    return accountApi.obtainAccount(cardNumber);
  }

  @Override
  public Single<AtmResponse> depositsAtm(String documentNumber) {
    return validatePerson(documentNumber)
        .doOnSuccess(personResponse -> {
          if (personResponse.getBlacklist().equals(Boolean.TRUE)) {
            throw new Exception("<----------------------Error, no existe el usuario!!!!!--------------------------------->");
          }
        })
        .flatMap(personResponse -> {
          if (personResponse.getFingerprint().equals(Boolean.TRUE)) {
            return validateFinger(personResponse.getDocument());
          } else {
            return validateReniec(personResponse.getDocument());
          }
        })
        .map(this::castValidation)
        .flatMap(successValidation -> obtainCard(documentNumber)
            .flatMap(cardResponse -> Flowable.fromIterable(cardResponse.getCards())
                .filter(Card::getActive)
                .flatMapSingle(card -> obtainAccount(card.getCardNumber()))
                .toList()
                .map(accountResponseList -> AtmResponse.builder()
                    .fingerprintEntityName(successValidation.getEntityName())
                    .validAccounts(accountResponseList.stream()
                        .map(accountResponse -> ValidAccountResponse.builder()
                            .accountNumber(accountResponse.getAccountNumber()).build())
                        .collect(Collectors.toList()))
                    .customerAmount(accountResponseList.stream()
                        .map(AccountResponse::getAmount)
                        .mapToDouble(Double::doubleValue)
                        .sum())
                    .build())));


  }



  private SuccessValidation castValidation(Object object) {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.convertValue(object, SuccessValidation.class);
  }
}
