package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.service;

import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response.AtmResponse;
import io.reactivex.Single;

public interface IAtmService {


  Single<AtmResponse> depositsAtm(String documentNumber);

}
