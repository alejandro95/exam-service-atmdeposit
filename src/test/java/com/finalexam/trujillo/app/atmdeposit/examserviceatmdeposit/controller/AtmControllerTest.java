package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.controller;

import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.request.PersonRequest;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response.AtmResponse;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response.ValidAccountResponse;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.service.IAtmService;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AtmControllerTest {

  @InjectMocks
  private AtmController atmController;

  @Mock
  private IAtmService tmService;

  @Test
  public void listDeposit() {

    when(tmService.depositsAtm(anyString())).thenReturn(Single.just(AtmResponse.builder()
        .fingerprintEntityName("CORE")
        .customerAmount(123.5)
        .validAccounts(Arrays.asList(ValidAccountResponse.builder()
                .accountNumber("1230")
                .build(),
            ValidAccountResponse.builder()
                .accountNumber("145454545")
                .build()))
        .build()));

    TestObserver<AtmResponse> atmResponseTestObserver = atmController.listDeposit(PersonRequest.builder()
        .documentNumber("1321354564")
        .build()).test();

    atmResponseTestObserver.awaitTerminalEvent();
    atmResponseTestObserver.assertComplete();
    atmResponseTestObserver.assertNoErrors();

  }
}
