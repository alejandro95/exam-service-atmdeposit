package com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.service;

import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.client.*;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.entity.Card;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.request.FingerPrintRequest;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.request.ReniecRequest;
import com.finalexam.trujillo.app.atmdeposit.examserviceatmdeposit.model.response.*;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AtmServiceImplTest {

  @InjectMocks
  private AtmServiceImpl atmService;

  @Mock
  private PersonApi personApi;

  @Mock
  private FingerprintApi fingerprintApi;

  @Mock
  private ReniecApi reniecApi;

  @Mock
  private CardApi cardApi;

  @Mock
  private AccountApi accountApi;

  @Test
  public void validatePersonTestFingerTrue() {

    when(personApi.getPerson(anyString())).thenReturn(Single.just(PersonResponse.builder()
        .document("10000000")
        .fingerprint(true)
        .blacklist(false)
        .build()));

    when(fingerprintApi.validateFingerprint(any(FingerPrintRequest.class))).thenReturn(Single.just(FingerPrintResponse.builder()
        .entityName("Core")
        .success(true)
        .build()));

    when(cardApi.obtainCard(anyString())).thenReturn(Single.just(CardResponse.builder()
        .cards(Arrays.asList(Card.builder().cardNumber("1111222233334441").active(true).build(),
            Card.builder().cardNumber("1111222233334442").active(true).build(),
            Card.builder().cardNumber("1111222233334443").active(true).build(),
            Card.builder().cardNumber("1111222233334444").active(false).build()))
        .build()));

    when(accountApi.obtainAccount(anyString())).thenReturn(Single.just(AccountResponse.builder()
        .accountNumber("12345")
        .amount(123.5)
        .build()));
    TestObserver<AtmResponse> atmResponseTestObserver = atmService.depositsAtm("1000000").test();

    atmResponseTestObserver.awaitTerminalEvent();
    atmResponseTestObserver.assertComplete();
    atmResponseTestObserver.assertNoErrors();

  }


  @Test
  public void validatePersonTestReniecTrue() {

    when(personApi.getPerson(anyString())).thenReturn(Single.just(PersonResponse.builder()
        .document("10000001")
        .fingerprint(false)
        .blacklist(false)
        .build()));

    when(reniecApi.validateReniec(any(ReniecRequest.class))).thenReturn(Single.just(ReniecResponse.builder()
        .entityName("Reniec")
        .success(true)
        .build()));

    when(cardApi.obtainCard(anyString())).thenReturn(Single.just(CardResponse.builder()
        .cards(Arrays.asList(Card.builder().cardNumber("1111222233334441").active(true).build(),
            Card.builder().cardNumber("1111222233334442").active(true).build(),
            Card.builder().cardNumber("1111222233334443").active(true).build(),
            Card.builder().cardNumber("1111222233334444").active(false).build()))
        .build()));

    when(accountApi.obtainAccount(anyString())).thenReturn(Single.just(AccountResponse.builder()
        .accountNumber("12345")
        .amount(123.5)
        .build()));
    TestObserver<AtmResponse> atmResponseTestObserver = atmService.depositsAtm("10000001").test();

    atmResponseTestObserver.awaitTerminalEvent();
    atmResponseTestObserver.assertComplete();
    atmResponseTestObserver.assertNoErrors();

  }

  @Test
  public void validatePersonTestBlackListTrue() {

    when(personApi.getPerson(anyString())).thenReturn(Single.just(PersonResponse.builder()
        .document("10000002")
        .fingerprint(true)
        .blacklist(true)
        .build()));

    TestObserver<AtmResponse> atmResponseTestObserver = atmService.depositsAtm("10000002").test();


    atmResponseTestObserver.assertError(Exception.class);


  }

}